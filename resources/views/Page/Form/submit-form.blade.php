@include('../../Layout/header')

<body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img
                src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820"
                alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>
    </div>
    <div class="preloader">
        <div class="cssload-container">
            <svg class="filter">
                <defs>
                    <filter id="gooeyness">
                        <fegaussianblur in="SourceGraphic" stddeviation="10" result="blur"></fegaussianblur>
                        <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -10"
                            result="gooeyness">
                        </fecolormatrix>
                        <fecomposite in="SourceGraphic" in2="gooeyness" operator="atop"></fecomposite>
                    </filter>
                </defs>
            </svg>
            <div class="dots">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>
        </div>
    </div>
    <!-- Page-->
    <div class="page">
        <!-- Page header-->
        @include('../../Layout/navbar')
        <!-- Parallax header-->
        <section class="section parallax-container context-dark" data-parallax-img="images/parallax-1.jpg">
            <div class="parallax-content parallax-header">
                <div class="parallax-header__inner context-dark">
                    <div class="parallax-header__content">
                        <div class="container">
                            <div class="row justify-content-sm-center">
                                <div class="col-md-10 col-xl-8">
                                    <h2 class="heading-decorated">Contacts</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-md bg-default decor-text" data-content="Contact">
            <div class="container">
                <div class="row row-50">
                    <div class="col-md-5 col-lg-4">
                        <h4 class="heading-decorated">Contact Details</h4>
                        <ul class="list-sm contact-info">
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>Address</dt>
                                    <dd>4578 Marmora Road, Glasgow, D04 89GR</dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>Phones</dt>
                                    <dd>
                                        <ul class="list-semicolon">
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                        </ul>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>We are open</dt>
                                    <dd>Mn-Fr: 10 am-8 pm</dd>
                                </dl>
                            </li>
                            <li>
                                <ul class="list-inline-sm">
                                    <li><a class="icon-sm fa-facebook icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-twitter icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-google-plus icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-vimeo icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-youtube icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-pinterest-p icon" href="#"></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-7 col-lg-8">
                        <h4 class="heading-decorated">Get in Touch</h4>
                        <!-- RD Mailform-->
                        <form class="rd-mailform rd-mailform_style-1 form-shadow" data-form-output="form-output-global"
                            data-form-type="contact" method="post" action="{{ route('send.email') }}">
                            @csrf
                            <div class="form-wrap form-wrap_icon linear-icon-man">
                                <input class="form-input" id="contact-name" type="text" name="name">
                                <label class="form-label" for="contact-name">Your name</label>
                            </div>
                            <div class="form-wrap form-wrap_icon linear-icon-envelope">
                                <input class="form-input" id="contact-email" type="email" name="user_email">
                                <label class="form-label" for="contact-email">Your e-mail</label>
                            </div>
                            <div class="form-wrap form-wrap_icon linear-icon-feather">
                                <textarea class="form-input" id="contact-message" name="message"></textarea>
                                <label class="form-label" for="contact-message">Your message</label>
                            </div>
                            <button class="button button-primary button-shadow" type="submit">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <!-- RD Google Map-->
            <div class="google-map-container" data-zoom="5" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45."
                data-styles="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:60}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:40},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;administrative.province&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:30}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ef8c25&quot;},{&quot;lightness&quot;:40}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b6c54c&quot;},{&quot;lightness&quot;:40},{&quot;saturation&quot;:-40}]},{}]">
                <div class="google-map"></div>
                <ul class="google-map-markers">
                    <li data-location="9870 St Vincent Place, Glasgow, DC 45 Fr 45."
                        data-description="9870 St Vincent Place, Glasgow" data-icon="images/gmap_marker.png"
                        data-icon-active="images/gmap_marker_active.png"></li>
                </ul>
            </div>
        </section>
        <!-- Page Footer-->
        <section class="pre-footer-corporate bg-image-7 bg-overlay-darkest">
            <div class="container">
                <div class="row justify-content-sm-center justify-content-lg-start row-30 row-md-60">
                    <div class="col-sm-10 col-md-6 col-lg-10 col-xl-3">
                        <h6>About</h6>
                        <p>theFuture is HTML template that fits for both developers and beginners. It comes loaded with
                            an
                            assortment of tools and features that make customization process much easier. A pack of
                            child themes,
                            specially designed for various business niches, allows users to create a fully functional
                            site for any
                            specific business quickly and worry-free.</p>
                    </div>
                    <div class="col-sm-10 col-md-6 col-lg-3 col-xl-3">
                        <h6>Navigation</h6>
                        <ul class="list-xxs">
                            <li><a href="#">Retina Homepage</a></li>
                            <li><a href="#">New Page Examples</a></li>
                            <li><a href="#">Parallax Sections</a></li>
                            <li><a href="#">Shortcode Central</a></li>
                            <li><a href="#">Ultimate Font Collection</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-md-6 col-lg-5 col-xl-3">
                        <h6>Recent Comments</h6>
                        <ul class="list-xs">
                            <li>
                                <!-- Comment minimal-->
                                <article class="comment-minimal">
                                    <p class="comment-minimal__author">Brian Williamson on</p>
                                    <p class="comment-minimal__link"><a href="standard-post.html">Site Speed and Search
                                            Engines
                                            Optimization Aspects</a></p>
                                </article>
                            </li>
                            <li>
                                <!-- Comment minimal-->
                                <article class="comment-minimal">
                                    <p class="comment-minimal__author">Brian Williamson on</p>
                                    <p class="comment-minimal__link"><a href="standard-post.html">5 Things to Know
                                            Before You Buy an HTML
                                            Template</a></p>
                                </article>
                            </li>
                            <li>
                                <!-- Comment minimal-->
                                <article class="comment-minimal">
                                    <p class="comment-minimal__author">Brian Williamson on</p>
                                    <p class="comment-minimal__link"><a href="standard-post.html">Turning Your Site into
                                            a Sales
                                            Machine</a></p>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-md-6 col-lg-4 col-xl-3">
                        <h6>Contacts</h6>
                        <ul class="list-xs">
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>Address</dt>
                                    <dd>4578 Marmora Road, Glasgow, D04 89GR</dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>Phones</dt>
                                    <dd>
                                        <ul class="list-semicolon">
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                        </ul>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>E-mail</dt>
                                    <dd><a href="mailto:#">info@demolink.org</a></dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>We are open</dt>
                                    <dd>Mn-Fr: 10 am-8 pm</dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        @include('../../Layout/footer')
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    @include('../../Layout/script')

</body>

</html>