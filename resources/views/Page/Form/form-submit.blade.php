@include('../../Layout/header')

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img
                src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820"
                alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>
    </div>
    <div class="preloader">
        <div class="cssload-container">
            <svg class="filter">
                <defs>
                    <filter id="gooeyness">
                        <fegaussianblur in="SourceGraphic" stddeviation="10" result="blur"></fegaussianblur>
                        <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -10"
                            result="gooeyness">
                        </fecolormatrix>
                        <fecomposite in="SourceGraphic" in2="gooeyness" operator="atop"></fecomposite>
                    </filter>
                </defs>
            </svg>
            <div class="dots">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>
        </div>
    </div>
    <!-- Page-->
    <div class="page">
        <!-- Page header-->
        @include('../../Layout/navbar')
        <!-- Parallax header-->
        <section class="section parallax-container context-dark" data-parallax-img="images/parallax-1.jpg">
            <div class="parallax-content parallax-header">
                <div class="parallax-header__inner context-dark">
                    <div class="parallax-header__content">
                        <div class="container">
                            <div class="row justify-content-sm-center">
                                <div class="col-md-10 col-xl-8">
                                    <h2 class="heading-decorated">Contacts</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-md bg-default decor-text" data-content="Contact">
            <div class="container">
                <div class="row row-50">
                    <div class="col-md-5 col-lg-4">
                        <h4 class="heading-decorated">Contact Details</h4>
                        <ul class="list-sm contact-info">
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>Address</dt>
                                    <dd>4578 Marmora Road, Glasgow, D04 89GR</dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>Phones</dt>
                                    <dd>
                                        <ul class="list-semicolon">
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                        </ul>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-inline">
                                    <dt>We are open</dt>
                                    <dd>Mn-Fr: 10 am-8 pm</dd>
                                </dl>
                            </li>
                            <li>
                                <ul class="list-inline-sm">
                                    <li><a class="icon-sm fa-facebook icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-twitter icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-google-plus icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-vimeo icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-youtube icon" href="#"></a></li>
                                    <li><a class="icon-sm fa-pinterest-p icon" href="#"></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-7 col-lg-8">
                        <h4 class="heading-decorated">Get in Touch</h4>
                        <!-- RD Mailform-->
                        <form id="emailForm" class="rd-mailform rd-mailform_style-1 form-shadow">
                            <!-- Your form fields go here -->
                            <label for="">Nama Lengkap</label>
                            <div class="form-wrap form-wrap_icon linear-icon-man mt-1">
                                <input class="form-input" id="full_name" type="text" name="full_name"
                                    placeholder="Nama Lengkap">
                            </div>

                            <label for="">Nomor Whatsapp</label>
                            <div class="form-wrap form-wrap_icon linear-icon-man mt-1">
                                <input class="form-input" id="wa_number" type="text" name="wa_number"
                                    placeholder="081222333444">
                            </div>

                            <label for="">E-mail</label>
                            <div class="form-wrap form-wrap_icon linear-icon-man mt-1">
                                <input class="form-input" id="email" type="email" name="email"
                                    placeholder="example@mail.com">
                            </div>

                            <label for="">Alamat Sesuai KTP</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="ktp_address" type="text" name="ktp_address"
                                    placeholder="Alamat Sesuai KTP">
                            </div>

                            <label for="">Alamat Domisili Sekarang</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="dom_address" type="text" name="dom_address"
                                    placeholder="Alamat Domisili Sekarang">
                            </div>

                            <label for="">No. Telpon Orang Tua</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="phone_ortu" type="text" name="phone_ortu"
                                    placeholder="081222333444 (Nama Orang Tua)">
                            </div>

                            <label for="">No. Telpon Saudara</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="phone_saudara" type="text" name="phone_saudara"
                                    placeholder="081222333444 (Nama Saudara)">
                            </div>

                            <label for="">No. Telpon Teman Terdekat</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="phone_close_friend" type="text" name="phone_close_friend"
                                    placeholder="081222333444 (Nama Teman Terdekat)">
                            </div>

                            <label for="">Universitas</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="university" type="text" name="university"
                                    placeholder="Nama Universitas">
                            </div>

                            <label for="">Fakultas</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="faculty" type="text" name="faculty"
                                    placeholder="Nama Fakultas">
                            </div>

                            <label for="">Jurusan</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="majoring" type="text" name="majoring"
                                    placeholder="Nama Jurusan (S1 Kedokteran)">
                            </div>

                            <label for="">Semester</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="semester" type="text" name="semester"
                                    placeholder="Semester">
                            </div>

                            <label for="">Program Study</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="prodi" type="text" name="prodi"
                                    placeholder="Program Study">
                            </div>

                            <label for="">Scan KTP</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="prodi" type="text" name="prodi"
                                    placeholder="Program Study">
                            </div>

                            <label for="">Scan KTM</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="prodi" type="text" name="prodi"
                                    placeholder="Program Study">
                            </div>

                            <label for="">NIM - Billing Pembayaran UKT</label>
                            <div class="form-wrap form-wrap_icon linear-icon-feather mt-1">
                                <input class="form-input" id="prodi" type="text" name="prodi"
                                    placeholder="NIM_Billing Pembayaran UKT">
                            </div>

                            <button class="button button-primary button-shadow" type="button" id="sendEmailBtn">
                                Send Email
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="pre-footer-corporate bg-image-7 bg-overlay-darkest">
            <div class="container">
                <div class="row justify-content-sm-center justify-content-lg-start row-30 row-md-60">
                    <div class="col-sm-10 col-md-6 col-lg-10 col-xl-3">
                        <h6>About</h6>
                        <p>theFuture is HTML template that fits for both developers and beginners. It comes loaded with
                            an
                            assortment of tools and features that make customization process much easier. A pack of
                            child themes,
                            specially designed for various business niches, allows users to create a fully functional
                            site for any
                            specific business quickly and worry-free.</p>
                    </div>
                    <div class="col-sm-10 col-md-6 col-lg-3 col-xl-3">
                        <h6>Navigation</h6>
                        <ul class="list-xxs">
                            <li><a href="#">Retina Homepage</a></li>
                            <li><a href="#">New Page Examples</a></li>
                            <li><a href="#">Parallax Sections</a></li>
                            <li><a href="#">Shortcode Central</a></li>
                            <li><a href="#">Ultimate Font Collection</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-md-6 col-lg-5 col-xl-3">
                        <h6>Recent Comments</h6>
                        <ul class="list-xs">
                            <li>
                                <!-- Comment minimal-->
                                <article class="comment-minimal">
                                    <p class="comment-minimal__author">Brian Williamson on</p>
                                    <p class="comment-minimal__link"><a href="standard-post.html">Site Speed and Search
                                            Engines
                                            Optimization Aspects</a></p>
                                </article>
                            </li>
                            <li>
                                <!-- Comment minimal-->
                                <article class="comment-minimal">
                                    <p class="comment-minimal__author">Brian Williamson on</p>
                                    <p class="comment-minimal__link"><a href="standard-post.html">5 Things to Know
                                            Before You Buy an HTML
                                            Template</a></p>
                                </article>
                            </li>
                            <li>
                                <!-- Comment minimal-->
                                <article class="comment-minimal">
                                    <p class="comment-minimal__author">Brian Williamson on</p>
                                    <p class="comment-minimal__link"><a href="standard-post.html">Turning Your Site into
                                            a Sales
                                            Machine</a></p>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-10 col-md-6 col-lg-4 col-xl-3">
                        <h6>Contacts</h6>
                        <ul class="list-xs">
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>Address</dt>
                                    <dd>4578 Marmora Road, Glasgow, D04 89GR</dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>Phones</dt>
                                    <dd>
                                        <ul class="list-semicolon">
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                            <li><a href="tel:#">(800) 123-0045</a></li>
                                        </ul>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>E-mail</dt>
                                    <dd><a href="mailto:#">info@demolink.org</a></dd>
                                </dl>
                            </li>
                            <li>
                                <dl class="list-terms-minimal">
                                    <dt>We are open</dt>
                                    <dd>Mn-Fr: 10 am-8 pm</dd>
                                </dl>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        @include('../../Layout/footer')
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    @include('../../Layout/script')

</body>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    $(document).ready(function () {
        $('#sendEmailBtn').on('click', function () {
            var formData = $('#emailForm').serialize();
            var csrfToken = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: '{{ route("send.email.ajax") }}', // Use Laravel named route
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                success: function (response) {
                    console.log(response);
                    // Handle success
                },
                error: function (error) {
                    console.error(error);
                    // Handle error
                }
            });
        });
    });
</script>