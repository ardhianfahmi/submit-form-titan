@include('../../Layout/header')

<body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img
                src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820"
                alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>
    </div>
    <div class="preloader">
        <div class="cssload-container">
            <svg class="filter">
                <defs>
                    <filter id="gooeyness">
                        <fegaussianblur in="SourceGraphic" stddeviation="10" result="blur"></fegaussianblur>
                        <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 20 -10"
                            result="gooeyness">
                        </fecolormatrix>
                        <fecomposite in="SourceGraphic" in2="gooeyness" operator="atop"></fecomposite>
                    </filter>
                </defs>
            </svg>
            <div class="dots">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>
        </div>
    </div>
    <!-- Page-->
    <div class="page">
        <section class="section-md bg-default decor-text d-flex flex-column justify-content-center"
            data-content="Features" style="height: 100%;">
            <div class="bg-decor d-flex align-items-center"
                data-parallax-scroll="{&quot;x&quot;: -50,  &quot;smoothness&quot;: 30}"><img
                    src="images/bg-decor-5.png" alt="" loading="lazy" />
            </div>
            <div class="container">
                <div class="row justify-content-md-center justify-content-lg-between row-50 align-items-center">
                    <div class="col-md-7 col-lg-4">
                        <figure class="image-sizing-1"
                            data-parallax-scroll="{&quot;y&quot;: -50,  &quot;smoothness&quot;: 30}"><img
                                src="images/home-business-1-1481x2068.png" alt="" width="1481" height="2068"
                                loading="lazy" />
                        </figure>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-6 position-relative">
                        <div class="card card-custom">
                            <div class="card-body">
                                <h4 class="heading-decorated">Login</h4>
                                <!-- Blurb minimal-->
                                <form class="rd-mailform rd-mailform_style-1 form-shadow" method="post" action="">
                                    <div class="form-wrap form-wrap_icon linear-icon-man">
                                        <input class="form-input" id="contact-name" type="text" name="username">
                                        <label class="form-label" for="contact-name">User Name</label>
                                    </div>
                                    <div class="form-wrap form-wrap_icon linear-icon-envelope">
                                        <input class="form-input" id="contact-email" type="password" name="password">
                                        <label class="form-label" for="contact-email">Password</label>
                                    </div>
                                    {{-- change type --}}
                                    <button class="button button-primary button-shadow" type="button"
                                        onclick="location.href='/form';">Login</button>
                                </form>
                                <p>Don't have an account? <a href="/register">Register</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="snackbars" id="form-output-global"></div>
</body>
@include('../../Layout/script')