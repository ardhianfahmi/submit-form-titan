<header class="page-header">
    <!-- RD Navbar-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
            data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static"
            data-xxl-layout="rd-navbar-static" data-device-layout="rd-navbar-fixed"
            data-sm-device-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed"
            data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static"
            data-xxl-device-layout="rd-navbar-static" data-xl-stick-up-offset="50px" data-xxl-stick-up-offset="50px">
            <!-- RD Navbar Top Panel-->
            <div class="rd-navbar-inner rd-navbar-search-wrap">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel rd-navbar-search-lg_collapsable">
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                    <!-- RD Navbar Brand-->
                    <div class="rd-navbar-brand">
                        <!--Brand--><a class="brand" href="index.html"><img class="brand-logo-dark"
                                src="images/logo-default-216x80.png" alt="" width="108" height="40"
                                loading="lazy" /><img class="brand-logo-light" src="images/logo-inverse-216x80.png"
                                alt="" width="108" height="40" loading="lazy" /></a>
                    </div>
                </div>
                <!-- RD Navbar Nav-->
                <div class="rd-navbar-nav-wrap rd-navbar-search_not-collapsable">
                    <!-- RD Navbar Nav-->
                    <div class="rd-navbar__element rd-navbar-search_collapsable">
                        <button class="rd-navbar-search__toggle rd-navbar-fixed--hidden"
                            data-rd-navbar-toggle=".rd-navbar-search-wrap"></button>
                    </div>
                    <!-- RD Search-->
                    <div class="rd-navbar-search rd-navbar-search_toggled rd-navbar-search_not-collapsable">
                        <form class="rd-search" action="search-results.html" method="GET"
                            data-search-live="rd-search-results-live">
                            <div class="form-wrap">
                                <input class="form-input" id="rd-navbar-search-form-input" type="text" name="s"
                                    autocomplete="off">
                                <label class="form-label" for="rd-navbar-search-form-input">Enter keyword</label>
                                <div class="rd-search-results-live" id="rd-search-results-live"></div>
                            </div>
                            <button class="rd-search__submit" type="submit"></button>
                        </form>
                        <div class="rd-navbar-fixed--hidden">
                            <button class="rd-navbar-search__toggle" data-custom-toggle=".rd-navbar-search-wrap"
                                data-custom-toggle-disable-on-blur="true"></button>
                        </div>
                    </div>
                    <div class="rd-navbar-search_collapsable">
                        <ul class="rd-navbar-nav">
                            <li class="rd-nav-item"><a class="rd-nav-link" href="index.html">Home</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-1.html">Home variant 1</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-2.html">Home variant 2</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-3.html">Home variant 3</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-4.html">Home variant 4</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-5.html">Home variant 5</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-6.html">Home variant 6</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-7.html">Home variant 7</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="home-variant-8.html">Home variant 8</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link">Features</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link">Headers</a>
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-default.html">Header Default</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-default-dark.html">Header Default Dark</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-modern.html">Header Modern</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-creative.html">Header Creative</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-corporate.html">Header Corporate</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-transparent.html">Header Transparent</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-minimal.html">Header Minimal</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-sidebar.html">Header Sidebar</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="header-sidebar-inverse.html">Header Sidebar Inverse</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link">Footers</a>
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="footer-default.html">Footer Default</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="footer-minimal.html">Footer Minimal</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="footer-modern.html">Footer Modern</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link" href="about.html">About</a>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link" href="services.html">Services</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="single-service.html">Single Service</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link">Portfolio</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-2-columns.html">Portfolio 2 columns</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-3-columns.html">Portfolio 3 columns</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-4-columns.html">Portfolio 4 columns</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-fullwidth.html">Portfolio Fullwidth</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-justify.html">Portfolio Justify</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-masonry.html">Portfolio Masonry</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="portfolio-album.html">Portfolio Album</a>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                            href="single-portfolio.html">Single Portfolio</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link">Pages</a>
                                <ul class="rd-menu rd-navbar-megamenu">
                                    <li class="rd-megamenu-item">
                                        <p class="rd-megamenu-header">General</p>
                                        <ul class="rd-megamenu-list">
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="about.html">About</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="our-team.html">Our Team</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="team-member-profile.html">Team Member Profile</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="services.html">Services</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="single-service.html">Single Service</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-megamenu-item">
                                        <p class="rd-megamenu-header">Special Pages</p>
                                        <ul class="rd-megamenu-list">
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="404-page.html">404 Page</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="search-results.html">Search Results</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="coming-soon.html">Coming Soon</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="privacy-policy.html">Privacy Policy</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-megamenu-item">
                                        <p class="rd-megamenu-header">Elements #1</p>
                                        <ul class="rd-megamenu-list">
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="accordion.html">Accordion</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="tabs.html">Tabs</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="buttons.html">Buttons</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="typography.html">Typography</a></li>
                                        </ul>
                                    </li>
                                    <li class="rd-megamenu-item">
                                        <p class="rd-megamenu-header">Elements #2</p>
                                        <ul class="rd-megamenu-list">
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="countdown.html">Countdown</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="animated-counter.html">Animated Counter</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="number-counter.html">Number Counter</a></li>
                                            <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link"
                                                    href="circles-counter.html">Circles Counter</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item"><a class="rd-nav-link">Blog</a>
                                <ul class="rd-menu rd-navbar-dropdown">
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link">Post Formats</a>
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="standard-post.html">Standard post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="nonstandard-post-with-sidebar.html">Nonstandard post with
                                                    sidebar</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="nonstandard-post.html">Nonstandard post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="gallery-post.html">Gallery post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="image-post.html">Image post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="standard-post.html">Link post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="quote-post.html">Quote post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="video-post.html">Video post</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="audio-post.html">Audio post</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link">Blog Layouts</a>
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="classic-blog.html">Classic Blog</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="grid-blog.html">Grid Blog</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="masonry-blog.html">Masonry Blog</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="justify-blog.html">Justify Blog</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="rd-dropdown-item"><a class="rd-dropdown-link">Sidebar</a>
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="left-sidebar.html">Left Sidebar</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="right-sidebar.html">Right Sidebar</a>
                                            </li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link"
                                                    href="no-sidebar.html">No Sidebar</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="rd-nav-item active"><a class="rd-nav-link" href="contacts.html">Contacts</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>