<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AuthController::class, 'login']);
Route::get('/register', [AuthController::class, 'register']);

// after login
Route::get('/form', [UserController::class, 'indexAjax']);
// SEND EMAIL
// Route::post('/send-mail', [MailController::class, 'sendMail']);

// Route::post('/send-email', [MailController::class, 'sendEmail'])->name('send.email');
// Route::post('/send-email-ajax', [MailController::class, 'sendEmailAjax'])->name('send.email.ajax');
Route::post('/send-email-ajax', [MailController::class, 'sendEmailAjax'])
    ->name('send.email.ajax')
    ->middleware('web');
