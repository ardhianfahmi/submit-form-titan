<?php

namespace App\Http\Controllers;

use Exception;
use Throwable;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\Desa\DesaController;
use App\Http\Controllers\Pengguna\Auth\PAuthController;

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Validator;
// use Tymon\JWTAuth\Facades\JWTAuth;
// use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function indexAjax()
    {
        return view('/Page/Form/form-submit');
    }
}
