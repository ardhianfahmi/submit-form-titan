<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MailController extends Controller
{
    public function sendEmail(Request $request)
    {
        $mail = new PHPMailer(true);

        try {
            // Server settings
            $mail->isSMTP();
            $mail->Host       = env('MAIL_HOST');
            $mail->SMTPAuth   = true;
            $mail->Username   = env('MAIL_USERNAME');
            $mail->Password   = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port       = env('MAIL_PORT');

            // Recipients
            $mail->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $mail->addAddress('ardhianfahmi.sabani072@gmail.com', 'Ardhian Sabani'); // Update recipient details here

            // Content
            $mail->isHTML(true);
            $mail->Subject = 'Contact Form Submission';
            $mail->Body    = "Name: {$request->input('name')}<br>Email: {$request->input('user_email')}<br>Message: {$request->input('message')}";

            $mail->send();
            return redirect()->back()->with('success', 'Email sent successfully!');
        } catch (Exception $e) {
            return redirect()->back()->with('error', "Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
        }
    }
    public function sendEmailAjax(Request $request)
    {
        try {
            // Create a new PHPMailer instance
            $mail = new PHPMailer(true);

            // Set up the SMTP server settings
            $mail->isSMTP();
            $mail->Host       = env('MAIL_HOST');
            $mail->SMTPAuth   = true;
            $mail->Username   = env('MAIL_USERNAME');
            $mail->Password   = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port       = env('MAIL_PORT');

            // Set the sender and recipient addresses
            $mail->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $mail->addAddress($request->input('user_email'), $request->input('name'));

            // Set email content
            $mail->isHTML(true);
            $mail->Subject = 'Subject of the Email';
            $mail->Body    = "Name: {$request->input('name')}<br>Email: {$request->input('user_email')}<br>Message: {$request->input('message')}";

            // Send the email
            $mail->send();

            return response()->json(['message' => 'Email sent successfully']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Email could not be sent', 'details' => $e->getMessage()], 500);
        }
    }
}
